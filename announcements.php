
<?php include 'part/head.php'; ?>
<body class="contianer">
<input type="checkbox" name="" id="nav-toggle" class="hidden-checkbox">
<div class="page">
	<?php include 'part/helper/no-js.php'; ?>
	<?php include 'part/helper/browsehappy.php'; ?>
	<?php include 'part/header.php'; ?>
<!-- container_main start -->		
<div class="container_article">
	<!-- aside -->
  <div data-col-aside="" class="article_box_l">
  	<div class="logo_img"><img src="assets/img/logo.jpg"></div>
  </div>
  <!-- aside end-->
	<!-- main start-->
  <div data-col-main="" class="article_box_r">
  	<h2>ANNOUNCEMENTS</h2>
    <article class="headline-content">
      <figure><a href=""><img src="assets/img/article_img.jpg" alt=""></a></figure>
     
        <div class="headline-box">
          <h3><a href="">
           Oct. 16 VLS organizes 'Say No to Domestic Violence' seminar </a></h3>   
               
           <p>The seminar addressed questions like "Why do spouses terrorize and torture their partners?" "Why is it that the vast majority of batterers are men and the vast majority of victims are women?"</p>
        </div>
   
    </article>
    <ul class="news-list">
      <li>
        <figure class="media">
          <img title="" src="http://images.christianpost.com/full/87894/lady-justice.jpg?w=138&amp;h=86&amp;l=50&amp;t=40">
        </figure>
        <div class="media-body">
          <h3> <a href="">Sept. 9. VLS will be closed on Sept. 11, 2010 in remembrance of 9/11 tragedy (US)</a></h3>
          Veritas Legal Society offices in the United States will be closed on September 11 to pay respect to those who lost their lives in the 9/11 attacks.</div>
      </li>
      <li>
        <figure class="media">
          <img title="" src="http://images.christianpost.com/full/87894/lady-justice.jpg?w=138&amp;h=86&amp;l=50&amp;t=40">
        </figure>
        <div class="media-body">
          <h3> <a href="">Sept. 9. VLS will be closed on Sept. 11, 2010 in remembrance of 9/11 tragedy (US)</a></h3>
          Veritas Legal Society offices in the United States will be closed on September 11 to pay respect to those who lost their lives in the 9/11 attacks.</div>
      </li>
      <li>
        <figure class="media">
          <img title="" src="http://images.christianpost.com/full/87894/lady-justice.jpg?w=138&amp;h=86&amp;l=50&amp;t=40">
        </figure>
        <div class="media-body">
          <h3> <a href="">Sept. 9. VLS will be closed on Sept. 11, 2010 in remembrance of 9/11 tragedy (US)</a></h3>
          Veritas Legal Society offices in the United States will be closed on September 11 to pay respect to those who lost their lives in the 9/11 attacks.</div>
      </li>
      <li>
        <figure class="media">
          <img title="" src="http://images.christianpost.com/full/87894/lady-justice.jpg?w=138&amp;h=86&amp;l=50&amp;t=40">
        </figure>
        <div class="media-body">
          <h3> <a href="">Sept. 9. VLS will be closed on Sept. 11, 2010 in remembrance of 9/11 tragedy (US)</a></h3>
          Veritas Legal Society offices in the United States will be closed on September 11 to pay respect to those who lost their lives in the 9/11 attacks.</div>
      </li>
      <li>
        <figure class="media">
          <img title="" src="http://images.christianpost.com/full/87894/lady-justice.jpg?w=138&amp;h=86&amp;l=50&amp;t=40">
        </figure>
        <div class="media-body">
          <h3> <a href="">Sept. 9. VLS will be closed on Sept. 11, 2010 in remembrance of 9/11 tragedy (US)</a></h3>
          Veritas Legal Society offices in the United States will be closed on September 11 to pay respect to those who lost their lives in the 9/11 attacks.</div>
      </li>
    </ul>
    <div class="page_more">READ MORE</div>


  </div>
	<!-- main end-->
  </div>
	<?php include 'part/footer.php'; ?>
</div>


<!-- scripts -->
<!-- <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script> -->
<script src="assets/js/script.js"></script>

</body>
</html>
