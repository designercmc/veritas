
<?php include 'part/head.php'; ?>
<body class="contianer">
<input type="checkbox" name="" id="nav-toggle" class="hidden-checkbox">
<div class="page">
	<?php include 'part/helper/no-js.php'; ?>
	<?php include 'part/helper/browsehappy.php'; ?>
	<?php include 'part/header.php'; ?>
<!-- container_main start -->		
<div class="container_article">
	<!-- aside -->
  <div data-col-aside="" class="article_box_l">
  	<div class="logo_img"><img src="assets/img/logo.jpg"></div>
  </div>
  <!-- aside end-->
	<!-- main start-->
  <div data-col-main="" class="article_box_r">
  	<article class="article-story">
  		<h1>Kandhamal Violence Update: 156 Acquitted, 1 Sentenced to Life Imprisonment</h1>
  		    <div class="byline-dateline">
  		   		<time datetime pubdate>September 25, 2015</time>
  		    </div>
  		    <figure><img src="assets/img/article_img.jpg" alt=""></a></figure></figure>
  		    <p>About 156 persons were acquitted by two fast track courts in Kandhamal last week.
  		The acquittal was in connection to three separate incidents that originated during the violence against Christians in Orissa's Kandhamal district in 2007 and August 2008.</p>
  		 <p>Citing lack of proper evidences, Additional Sessions Judge SK Das of Fast Track Court-I acquitted 14 persons in a house burning incident at village Dakedi under G Udayagiri Police station.
  		In another case, the Additional Sessions Judge of Fast Court-II acquitted all the 142 persons arrested by the Brahmunigam police in two cases of torching houses during the violence in 2007, according to the Press Trust of India.</p>
  		<p>In the midst of the large number of acquittals, only one person was convicted and sentenced to life imprisonment.</p>
  		<p>Fast Track Court-I convicted Kartik Parmanik for the murder of Ramesh Digal of Dakarpanga village in the Raikia police station area on August 25, 2008.
  		Additional Sessions Judge SK Das sentenced Parmanik to life and imposed a fine of Rs 5,000.
  		Raikia police had earlier arrested Parmanik after a complaint was lodged by the victim’s brother, Naresh Digal.</p>
  		<p>According to sources, Ramesh was murdered while fleeing with his family when his house was set on fire.</p>
  		<p>The communally-sensitive Kandhamal district had witnessed largescale violence in the aftermath of the killing of VHP leader Laxmanananda Saraswati.
  		More than 50 Christians were killed and at least 50,000 displaced during the violence that continued for over two months.</p>
  		<p>It is reported that out of 3,300 complaints filed by victims in the local police stations only 831 have been registered as FIRs.
  		Last week, the U.S. Commission on International Religious Freedom, placed India on the ‘Watch List’ of countries with severe violations of religious freedom, primarily due to lack of progress in achieving justice for victims of past large-scale incidents of communal violence.</p>
  		</p>The independent federal agency has called on US to urge the Indian government to strengthen the ability of state and central police to provide effective measures to prohibit and punish cases of religious violence to the fullest extent of the law while protecting victims and witnesses.</p>
  		
  		  </div>
  	</div>
	<!-- main end-->
  </article>
	<?php include 'part/footer.php'; ?>
</div>


<!-- scripts -->
<!-- <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script> -->
<script src="assets/js/script.js"></script>

</body>
</html>
