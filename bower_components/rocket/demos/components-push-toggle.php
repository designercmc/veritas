<?php include 'include/head.php'; ?>
<body>
<div class="page">
  <div class="container">

    <div class="topic">
      <h2 id=""><span>components: </span>Push toggle</h2>
      <div class="example">
        <div class="push-toggle two">
          <input type="radio" id="male" name="gender" checked="">
          <label for="male">Male</label>
          <input type="radio" id="female" name="gender">
          <label for="female">Female</label>
        </div>              
        <div class="push-toggle three">
          <input type="radio" id="one" name="grade" checked="">
          <label for="one">One</label>
          <input type="radio" id="two" name="grade">
          <label for="two">Two</label>
          <input type="radio" id="three" name="grade">
          <label for="three">Three</label>
        </div>              
      </div>
      <pre><code class="language-scss">

      </code></pre>
    
    <?php include "include/more-demos.php" ?>
  </div>
  <?php include "include/site-footer.php"; ?>
</div>
</body>
</html>

