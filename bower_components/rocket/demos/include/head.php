<?php
  $file = basename($_SERVER['PHP_SELF']);
  $pagename = str_replace(".php","",$file); 
?>
<!doctype html>
<!--[if lt IE 7]><html class="no-js lt-ie10 lt-ie9 lt-ie8 lt-ie7" lang="en"><![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html class="no-js lt-ie10 lt-ie9 lt-ie8" lang="en"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html class="no-js lt-ie10 lt-ie9" lang="en"><![endif]-->
<!--[if (IE 9)&!(IEMobile)]><html class="no-js lt-ie10" lang="en"><![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js" lang="en"><!--<![endif]-->
<head>
	<!-- Change this to match your local server hostname and path -->
	<!-- <base href="http://localhost:8888/new/codeset/"> --><!--[if lte IE 6]></base><![endif]-->

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=no">
	<!-- <meta http-equiv="cleartype" content="on"> -->
  <title> <?php 
		if ($pagename == 'index') {
			echo 'Rocket';
		} 
		if ($pagename == 'layout-grid') {
			echo 'layout-grid';
		} 
		if ($pagename == 'layout-gallery') {
			echo 'layout-gallery';
		} 
		if ($pagename == 'layout-justify') {
			echo 'layout-justify';
		} 
		if ($pagename == 'layout-center') {
			echo 'layout-center';
		} 
		if ($pagename == 'layout-two-columns') {
			echo 'layout-two-columns';
		} 
		if ($pagename == 'layout-debug') {
			echo 'layout-debug';
		} 
		if ($pagename == 'slider-carousel') {
			echo 'slider-carousel';
		} 
		if ($pagename == 'slider-gallery') {
			echo 'slider-gallery';
		} 
		if ($pagename == 'components-button') {
			echo 'components-button';
		} 
		if ($pagename == 'components-media-list') {
			echo 'components-media-list';
		} 
		if ($pagename == 'components-offcanvas') {
			echo 'components-offcanvas';
		} 
		if ($pagename == 'components-mobile-nav-slide-in') {
			echo 'components-mobile-nav-slide-in';
		} 
		if ($pagename == 'components-mobile-nav-slide-along') {
			echo 'components-mobile-nav-slide-along';
		} 
		if ($pagename == 'components-mobile-nav-slide-out') {
			echo 'components-mobile-nav-slide-out';
		} 
		if ($pagename == 'components-mobile-nav-rotate-in') {
			echo 'components-mobile-nav-rotate-in';
		} 
		if ($pagename == 'components-mobile-nav-rotate-out') {
			echo 'components-mobile-nav-rotate-out';
		} 
		if ($pagename == 'components-mobile-nav-rotate-in-reverse') {
			echo 'components-mobile-nav-rotate-in-reverse';
		} 
		if ($pagename == 'components-mobile-nav-scale-down') {
			echo 'components-mobile-nav-scale-down';
		} 
		if ($pagename == 'components-mobile-nav-scale-up') {
			echo 'components-mobile-nav-scale-up';
		} 
		if ($pagename == 'components-mobile-nav-open') {
			echo 'components-mobile-nav-open';
		} 
		if ($pagename == 'components-mobile-nav-push') {
			echo 'components-mobile-nav-push';
		} 
		if ($pagename == 'components-mobile-nav-reveal') {
			echo 'components-mobile-nav-reveal';
		} 
		if ($pagename == 'components-mobile-nav-drawer') {
			echo 'components-mobile-nav-drawer';
		} 
		if ($pagename == 'components-dropdown') {
			echo 'components-dropdown';
		} 
		if ($pagename == 'components-tabs') {
			echo 'components-tabs';
		} 
		if ($pagename == 'components-push-toggle') {
			echo 'components-push-toggle';
		} 
		if ($pagename == 'components-switch') {
			echo 'components-switch';
		} 
		if ($pagename == 'components-accordion') {
			echo 'components-accordion';
		} 
		if ($pagename == 'components-checkbox') {
			echo 'components-checkbox';
		} 
		if ($pagename == 'components-tooltip') {
			echo 'components-tooltip';
		} 
		if ($pagename == 'components-flex-video') {
			echo 'components-flex-video';
		} 
		if ($pagename == 'components-slider-carousel') {
			echo 'components-slider-carousel';
		} 
		if ($pagename == 'components-slider-gallery') {
			echo 'components-slider-gallery';
		} 
		if ($pagename == 'addons-type') {
			echo 'addons-type';
		} 
		if ($pagename == 'addons-font-size') {
			echo 'addons-font-size';
		} 
		if ($pagename == 'addons-visibility') {
			echo 'addons-visibility';
		} 
		if ($pagename == 'addons-breakpoint') {
			echo 'addons-breakpoint';
		} 
		if ($pagename == 'color-contrast') {
			echo 'color-contrast';
		} 
		if ($pagename == 'color-adjacent') {
			echo 'color-adjacent';
		} 
		if ($pagename == 'color-complementary') {
			echo 'color-complementary';
		} 
		if ($pagename == 'color-split-complementary') {
			echo 'color-split-complementary';
		} 
		if ($pagename == 'color-triad') {
			echo 'color-triad';
		} 
		if ($pagename == 'color-rectangle') {
			echo 'color-rectangle';
		} 
		if ($pagename == 'color-square') {
			echo 'color-square';
		} 
  ?> </title>
	
	<meta name="description" content="">
	<meta name="author" content="">
	<meta name="HandheldFriendly" content="True">
	<meta name="MobileOptimized" content="320">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">

	<!-- Prefetch DNS for external assets -->
	<link href="http://fonts.googleapis.com" rel="dns-prefetch">
	
	<!-- css -->
	<link href="https://fontastic.s3.amazonaws.com/MSJHPuJFXkve8cKEDAVMKT/icons.css" rel="stylesheet">
	<link href='http://fonts.googleapis.com/css?family=Roboto:700,500,300' rel='stylesheet' type='text/css'>
	<link href="prism/prism.css" rel="stylesheet">
	<link href="css/normalize.css" rel="stylesheet">

  <link <?php 
		if ($pagename == 'index') {
			echo 'href="css/test.css"';
		} 
		if ($pagename == 'layout-grid') {
			echo 'href="css/layout-grid.css"';
		} 
		if ($pagename == 'layout-gallery') {
			echo 'href="css/layout-gallery.css"';
		} 
		if ($pagename == 'layout-justify') {
			echo 'href="css/layout-justify.css"';
		} 
		if ($pagename == 'layout-center') {
			echo 'href="css/layout-center.css"';
		} 
		if ($pagename == 'layout-two-columns') {
			echo 'href="css/layout-two-columns.css"';
		} 
		if ($pagename == 'layout-debug') {
			echo 'href="css/layout-debug.css"';
		} 
		if ($pagename == 'slider-carousel') {
			echo 'href="css/slider-carousel.css"';
		} 
		if ($pagename == 'slider-gallery') {
			echo 'href="css/slider-gallery.css"';
		} 
		if ($pagename == 'components-button') {
			echo 'href="css/components-button.css"';
		} 
		if ($pagename == 'components-media-list') {
			echo 'href="css/components-media-list.css"';
		} 
		if ($pagename == 'components-offcanvas') {
			echo 'href="css/components-offcanvas.css"';
		} 
		if ($pagename == 'components-mobile-nav-slide-in') {
			echo 'href="css/components-mobile-nav-slide-in.css"';
		} 
		if ($pagename == 'components-mobile-nav-slide-along') {
			echo 'href="css/components-mobile-nav-slide-along.css"';
		} 
		if ($pagename == 'components-mobile-nav-slide-out') {
			echo 'href="css/components-mobile-nav-slide-out.css"';
		} 
		if ($pagename == 'components-mobile-nav-rotate-in') {
			echo 'href="css/components-mobile-nav-rotate-in.css"';
		} 
		if ($pagename == 'components-mobile-nav-rotate-out') {
			echo 'href="css/components-mobile-nav-rotate-out.css"';
		} 
		if ($pagename == 'components-mobile-nav-rotate-in-reverse') {
			echo 'href="css/components-mobile-nav-rotate-in-reverse.css"';
		} 
		if ($pagename == 'components-mobile-nav-scale-down') {
			echo 'href="css/components-mobile-nav-scale-down.css"';
		} 
		if ($pagename == 'components-mobile-nav-scale-up') {
			echo 'href="css/components-mobile-nav-scale-up.css"';
		} 
		if ($pagename == 'components-mobile-nav-open') {
			echo 'href="css/components-mobile-nav-open.css"';
		} 
		if ($pagename == 'components-mobile-nav-push') {
			echo 'href="css/components-mobile-nav-push.css"';
		} 
		if ($pagename == 'components-mobile-nav-reveal') {
			echo 'href="css/components-mobile-nav-reveal.css"';
		} 
		if ($pagename == 'components-mobile-nav-drawer') {
			echo 'href="css/components-mobile-nav-drawer.css"';
		} 
		if ($pagename == 'components-dropdown') {
			echo 'href="css/components-dropdown.css"';
		} 
		if ($pagename == 'components-tabs') {
			echo 'href="css/components-tabs.css"';
		} 
		if ($pagename == 'components-switch') {
			echo 'href="css/components-switch.css"';
		} 
		if ($pagename == 'components-accordion') {
			echo 'href="css/components-accordion.css"';
		} 
		if ($pagename == 'components-push-toggle') {
			echo 'href="css/components-push-toggle.css"';
		} 
		if ($pagename == 'components-checkbox') {
			echo 'href="css/components-checkbox.css"';
		} 
		if ($pagename == 'components-tooltip') {
			echo 'href="css/components-tooltip.css"';
		} 
		if ($pagename == 'components-flex-video') {
			echo 'href="css/components-flex-video.css"';
		} 
		if ($pagename == 'components-slider-carousel') {
			echo 'href="css/components-slider-carousel.css"';
		} 
		if ($pagename == 'components-slider-gallery') {
			echo 'href="css/components-slider-gallery.css"';
		} 
		if ($pagename == 'addons-type') {
			echo 'href="css/addons-type.css"';
		} 
		if ($pagename == 'addons-font-size') {
			echo 'href="css/addons-font-size.css"';
		} 
		if ($pagename == 'addons-visibility') {
			echo 'href="css/addons-visibility.css"';
		} 
		if ($pagename == 'addons-breakpoint') {
			echo 'href="css/addons-breakpoint.css"';
		} 
		if ($pagename == 'color-contrast') {
			echo 'href="css/color-contrast.css"';
		} 
		if ($pagename == 'color-adjacent') {
			echo 'href="css/color-adjacent.css"';
		} 
		if ($pagename == 'color-complementary') {
			echo 'href="css/color-complementary.css"';
		} 
		if ($pagename == 'color-split-complementary') {
			echo 'href="css/color-split-complementary.css"';
		} 
		if ($pagename == 'color-triad') {
			echo 'href="css/color-triad.css"';
		} 
		if ($pagename == 'color-rectangle') {
			echo 'href="css/color-rectangle.css"';
		} 
		if ($pagename == 'color-square') {
			echo 'href="css/color-square.css"';
		} 
  ?> rel="stylesheet">

	<!-- javascript -->
	<!--[if (lt IE 9)]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<script src="js/ie.min.js"></script>
		
		<link href="http://externalcdn.com/respond-proxy.html" id="respond-proxy" rel="respond-proxy" />
		<link href="cross-domain/respond.proxy.gif" id="respond-redirect" rel="respond-redirect" />
		<script src="cross-domain/respond.proxy.js"></script>	
	<![endif]-->

	<script src="prism/prism.js"></script>
	<script src="js/Modernizr.js"></script>
	<script src="js/rem.min.js"></script>
	<script src="js/index.min.js"></script>
</head>
