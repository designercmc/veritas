<div class="more-demos">
  <h2>More demos</h2>
  <h4>layout</h4>
  <nav class="demo-links">
    <a <?php if ($pagename == 'layout-grid' ) { echo 'class="current"'; } ?> href="layout-grid.php">grid</a>
    <a <?php if ($pagename == 'layout-gallery' ) { echo 'class="current"'; } ?> href="layout-gallery.php">gallery</a>
    <a <?php if ($pagename == 'layout-justify' ) { echo 'class="current"'; } ?> href="layout-justify.php">justify</a>
    <a <?php if ($pagename == 'layout-center' ) { echo 'class="current"'; } ?> href="layout-center.php">center</a>
    <a <?php if ($pagename == 'layout-two-columns' ) { echo 'class="current"'; } ?> href="layout-two-columns.php">two-columns</a>
  </nav>
  <h4>components</h4>
  <nav class="demo-links">
    <a <?php if ($pagename == 'components-button' ) { echo 'class="current"'; } ?> href="components-button.php">button</a>
    <a <?php if ($pagename == 'components-media-list' ) { echo 'class="current"'; } ?> href="components-media-list.php">media-list</a>
    <a <?php if ($pagename == 'components-offcanvas' ) { echo 'class="current"'; } ?> href="components-offcanvas.php">offcanvas</a>
    <a <?php if ($pagegroup == 'mobile-nav' ) { echo 'class="current"'; } ?> href="components-mobile-nav-slide-in.php">mobile-nav</a>
    <a <?php if ($pagename == 'components-dropdown' ) { echo 'class="current"'; } ?> href="components-dropdown.php">dropdown</a>
    <a <?php if ($pagename == 'components-tabs' ) { echo 'class="current"'; } ?> href="components-tabs.php">tabs</a>
    <a <?php if ($pagename == 'components-switch' ) { echo 'class="current"'; } ?> href="components-switch.php">switch</a>
    <a <?php if ($pagename == 'components-accordion' ) { echo 'class="current"'; } ?> href="components-accordion.php">accordion</a>
    <a <?php if ($pagename == 'components-push-toggle' ) { echo 'class="current"'; } ?> href="components-push-toggle.php">push-toggle</a>
    <a <?php if ($pagename == 'components-checkbox' ) { echo 'class="current"'; } ?> href="components-checkbox.php">checkbox</a>
    <a <?php if ($pagename == 'components-tooltip' ) { echo 'class="current"'; } ?> href="components-tooltip.php">tooltip</a>
    <a <?php if ($pagename == 'components-flex-video' ) { echo 'class="current"'; } ?> href="components-flex-video.php">flex-video</a>
    <a <?php if ($pagename == 'components-slider-carousel' ) { echo 'class="current"'; } ?> href="components-slider-carousel.php">slider-carousel</a>
    <a <?php if ($pagename == 'components-slider-gallery' ) { echo 'class="current"'; } ?> href="components-slider-gallery.php">slider-gallery</a>
  </nav>
  <h4>addons</h4>
  <nav class="demo-links">
    <a <?php if ($pagename == 'addons-type' ) { echo 'class="current"'; } ?> href="addons-type.php">type</a>
    <a <?php if ($pagename == 'addons-font-size' ) { echo 'class="current"'; } ?> href="addons-font-size.php">font-size</a>
    <a <?php if ($pagename == 'addons-visibility' ) { echo 'class="current"'; } ?> href="addons-visibility.php">visibility</a>
    <a <?php if ($pagename == 'addons-breakpoint' ) { echo 'class="current"'; } ?> href="addons-breakpoint.php">breakpoint</a>
  </nav>
  <h4>color functions</h4>
  <nav class="demo-links">
    <a <?php if ($pagename == 'color-contrast' ) { echo 'class="current"'; } ?> href="color-contrast.php">contrast</a>
    <a <?php if ($pagename == 'color-adjacent' ) { echo 'class="current"'; } ?> href="color-adjacent.php">adjacent</a>
    <a <?php if ($pagename == 'color-complementary' ) { echo 'class="current"'; } ?> href="color-complementary.php">complementary</a>
    <a <?php if ($pagename == 'color-split-complementary' ) { echo 'class="current"'; } ?> href="color-split-complementary.php">split-complementary</a>
    <a <?php if ($pagename == 'color-triad' ) { echo 'class="current"'; } ?> href="color-triad.php">triad</a>
    <a <?php if ($pagename == 'color-rectangle' ) { echo 'class="current"'; } ?> href="color-rectangle.php">rectangle</a>
    <a <?php if ($pagename == 'color-square' ) { echo 'class="current"'; } ?> href="color-square.php">square</a>
  </nav>
</div>
