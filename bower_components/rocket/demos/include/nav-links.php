<div class="nav-links">
  <a href="components-mobile-nav-slide-in.php" class="btn <?php if ($pagename == 'components-mobile-nav-slide-in') { echo 'active'; } ?>">slide-in</a>
  <a href="components-mobile-nav-slide-along.php" class="btn <?php if ($pagename == 'components-mobile-nav-slide-along') { echo 'active'; } ?>">slide-along</a>
  <a href="components-mobile-nav-slide-out.php" class="btn <?php if ($pagename == 'components-mobile-nav-slide-out') { echo 'active'; } ?>">slide-out</a>
  <a href="components-mobile-nav-rotate-in.php" class="btn <?php if ($pagename == 'components-mobile-nav-rotate-in') { echo 'active'; } ?>">rotate-in</a>
  <a href="components-mobile-nav-rotate-out.php" class="btn <?php if ($pagename == 'components-mobile-nav-rotate-out') { echo 'active'; } ?>">rotate-out</a>
  <a href="components-mobile-nav-rotate-in-reverse.php" class="btn <?php if ($pagename == 'components-mobile-nav-rotate-in-reverse') { echo 'active'; } ?>">rotate-in-reverse</a>
  <a href="components-mobile-nav-scale-down.php" class="btn <?php if ($pagename == 'components-mobile-nav-scale-down') { echo 'active'; } ?>">scale-down</a>
  <a href="components-mobile-nav-scale-up.php" class="btn <?php if ($pagename == 'components-mobile-nav-scale-up') { echo 'active'; } ?>">scale-up</a>
  <a href="components-mobile-nav-open.php" class="btn <?php if ($pagename == 'components-mobile-nav-open') { echo 'active'; } ?>">open</a>
  <a href="components-mobile-nav-push.php" class="btn <?php if ($pagename == 'components-mobile-nav-push') { echo 'active'; } ?>">push</a>
  <a href="components-mobile-nav-reveal.php" class="btn <?php if ($pagename == 'components-mobile-nav-reveal') { echo 'active'; } ?>">reveal</a>
  <a href="components-mobile-nav-drawer.php" class="btn <?php if ($pagename == 'components-mobile-nav-drawer') { echo 'active'; } ?>">drawer</a>
</div>