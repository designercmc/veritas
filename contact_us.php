<?php include 'part/head.php'; ?>
<body class="contianer">
<input type="checkbox" name="" id="nav-toggle" class="hidden-checkbox">
<div class="page">
	<?php include 'part/helper/no-js.php'; ?>
	<?php include 'part/helper/browsehappy.php'; ?>
	<?php include 'part/header.php'; ?>
<!-- container_main start -->		
<div class="container_article">
	<!-- aside -->
  <div data-col-aside="" class="article_box_l">
  	<div class="logo_img"><img src="assets/img/logo.jpg"></div>
  </div>
  <!-- aside end-->
	<!-- main start-->
  <div data-col-main="" class="article_box_r">
  	  <h2>Contact Us</h2>
  <div class="h3">If you want Veritas Legal  Society to help you enrich your life as a believer, please write to us at:  </div>
  <div class="info_box ">Veritas Legal Society<br />
    529 14th St. NW Ste. 770<br />
    Washington DC 20045<br />
    USA</div>

  <div class="mail">Or email us at: <a href="mailto:veritaslegalsociety@gmail.com">veritaslegalsociety@gmail.com</a>
  </div>

  </div>
	<!-- main end-->
  </div>
	<?php include 'part/footer.php'; ?>
</div>


<!-- scripts -->
<!-- <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script> -->
<script src="assets/js/script.js"></script>

</body>
</html>
