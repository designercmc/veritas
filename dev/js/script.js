// codekit-prepend "../../bower_components/REM-unit-polyfill/js/rem.js"
// codekit-prepend "../../bower_components/responsive-img.js/responsive-img.js"
// @codekit-prepend "lib/Modernizr.js"
// @codekit-prepend "lib/svg4everybody.ie8.js"
// @codekit-prepend "../../bower_components/rocket/src/js/kit.js"

ready(function () {
  k('.search-toggle').click(function() {
    k('.head-search').toggleClass('search-show');
  });
  })

   // home slider
  sliderAutoplay('.home-slider', 5000);
});
