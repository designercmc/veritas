<?php include 'part/head.php'; ?>
<body >
<input type="checkbox" name="" id="nav-toggle" class="hidden-checkbox">
<div class="page">
	<?php include 'part/helper/no-js.php'; ?>
	<?php include 'part/helper/browsehappy.php'; ?>
<!--header start -->
<!--[if (!IE 8)&(!IE 7)]><!-->
<!--<![endif]-->
<!-- site-head start -->

<header class="site-head ">
	<div class="container">

		<!-- wrap-head-dk start -->
 		    <div class="wrap-head">
				
						<h1 class="logo"><a href="index.php"><img src="assets/img/logo-lg.png" alt="Veritas" ></a></h1>   
						 <div class="wrap-head-dk">
						 	     <nav class="nav">
	 								      <ul>
	 								        <li><a href="index.php">Home</a></li>
	 								        <li><a href="who_we_are.php">Who We Are</a></li>
	 								        <li><a href="what_we_do.php">What We Do</a></li>
	 								        <li><a href="affiliations.php">Affiliations</a></li>
	 								        <li><a href="membership.php">Membership</a></li>
	 								        <li><a href="contact_us.php">Contact Us</a></li>
	 								      </ul>
	 								    </nav>       
	 	
	 							<!-- wrap-tool start -->
	 							<div class="wrap-tool">
	 								<div class="wrap-tool-mb">
	 									<label for="nav-toggle"><i class="i-menu"></i></label>
	 									<label for="nav-toggle" class="page-overlay"></label>
	 								    
	 								    <form action="" class="head-search">
							 	           <input type="text" placeholder="Search" id="head-search" autofocus>
							 	           <input type="submit" hidefocus="true" class="hidden">
							 	           <div class="search-toggle">
							 	           	<i class="i-search"></i>
							 	           </div>
	 						            </form>
	 						         </div>   
	 	  						  </div>
	 	  						  <!-- wrap-tool end-->
						   </div>
					</div>    
			   

	<!-- site-head end -->	
</header>
<!--wrap-site-head end -->


	<div class="slider-wrap">
						<div class="home-slider">
							  <input type="radio" name="banner" id="banner-1" checked="">
							  <input type="radio" name="banner" id="banner-2">
							  <input type="radio" name="banner" id="banner-3">
							  <input type="checkbox" name="banner-autoplay" id="banner-autoplay" checked="">
							  <div class="outer">
							    <ul class="inner">
							     <li>
					              <figure>
					                <a href=""><img src="assets/img/banner03.jpg" alt=""></a>
					              </figure>
					              <div class="text">
					                <h2><a href="">Helping Legal professionals find answer to GOD’s calling in their lives and securing justice.</a></h2>
					              </div>
	          		     </li>
	          			 <li>
					              <figure>
					                <a href=""><img src="assets/img/banner02.jpg" alt=""></a>
					              </figure>
					              <div class="text">
					                <h2><a href="">Helping Legal professionals find answer to GOD’s calling in their lives and securing justice.</a></h2>
					              </div>
	          		     </li>
	          		     <li>
					              <figure>
					                <a href=""><img src="assets/img/banner01.jpg" alt=""></a>
					              </figure>
					              <div class="text">
					                <h2><a href="">Helping Legal professionals find answer to GOD’s calling in their lives and securing justice.</a></h2>
					              </div>
	          		     </li>
							    </ul>
							  </div>
							  <!-- <div class="controls">
							    <label for="banner-1"><span class="prev"></span><span class="next"></span></label>
							    <label for="banner-2"><span class="prev"></span><span class="next"></span></label>
							    <label for="banner-3"><span class="prev"></span><span class="next"></span></label>
							  </div>
							  <div class="dots">
							    <label for="banner-1"><span class="normal"></span><span class="active"></span></label>
							    <label for="banner-2"><span class="normal"></span><span class="active"></span></label>
							    <label for="banner-3"><span class="normal"></span><span class="active"></span></label>
							  </div>  -->
							  <div class="autoplay">
							    <div class="autoplay-progress"></div>
							    <label for="banner-autoplay"><span class="play"></span><span class="pause"></span></label>
							  </div>
		 				</div>	
			     </div>



<!-- container_main start --> 
<div class="container_main">
	<div class="main_box01"> 
		<div class="logo_img"><img src="assets/img/logo.jpg"></div>
	</div>
	<div class="main_box02"> 
		<h2><a href="announcements.php">ANNOUNCEMENTS</a></h2>
		<div class="main_box01_l ">
		    <h3><a href="">Sep 1, 2015 Become One of Our On-call Advisors</a></h3>
			<p>VLS needs volunteers with subject matter specialization in specific areas of law who can be present at or reachable by phone during legal aid desk sessions to guide volunteer attorneys on specific issues. </p>
			<h3><a href="">Sep 27, 2015 3:00pm Legal Aid Desk – Immanuel Community Church </a></h3>
			<p>VLS works with Immanuel Community Church located at 6 Barclay Street, 4th Floor New York NY 10007 to provide free legal services.</p>
	    </div>
	    <div class="main_box01_r">
			<h3><a href="">Oct 1, 2015 3:00pm VLS Holds Executive Meeting</a></h3>
			<p>The executive committee members of VLS held a meeting to prepare the roadmap for 2016. The members will share concerns about human rights atrocities in Syria, Uganda and Egypt and discuss strategies for spreading legal awareness among the needy p</p>
			<h3><a href="">Oct 15, 2015 10:00am New Volunteer Fellowship</a></h3>
			<p>Open Hands volunteers provide free legal counseling and representation to homeless and low-income New Yorkers on a variety of legal issues.  Come have fellowship with our volunteers and equip yourself for the next Legal Aid Desk.</p>
		</div>	
	</div>
</div>
<!-- container_main end -->
<!-- container_main start -->		
<div class="container_main">
	
	<div class="main_box01"> 
		<div class="h2"><a href="legal_aid.php">LEGAL AID</a></div>
		<figure><img src="assets/img/event.jpg"></figure>
		<!-- <div class=h3"">ON THE HOMEPAGE</div> -->
		<h3><a href="">An important duty of Veritas Legal Society is to dispense legal aid.</a></h3>
		<p>Members of Veritas Legal Society are mandated to offer basic legal advice and services to disadvantaged and needy believers without charging any fees.</br> Our typical legal aid work involves counseling, informal negotiation, and appearances in administrative hearings</br> Verital Legal Society legal aid program includes but is not confined to the following fields of law</p>
		<div class="mail"><a href="mailto:legalaid@veritaslegalsociety.org"> legalaid@veritaslegalsociety.org</a></div>
	</div>
	<div class="main_box03"> 
		<h2><a href="news_alerts.php">NEWS ALERTS</a></h2>
		<h3><a href="">Catholics, Evangelicals, Jews Join Forces, Ask Supreme Court to Take Obamacare Contraceptive Mandate Case</a></h3>
		<p>Multinational businesses involved in complex cross-border restructurings, workouts </p>
		<h3><a href="">raqi authorities document Christian persecution for the first time</a></h3>
		<p>Multinational businesses involved in complex cross-border restructurings, workouts </p>
		<h3><a href="">Iran Church Flourishing Despite Repression</a></h3>
		<p>Multinational businesses involved in complex cross-border restructurings, workouts </p>
		<h3><a href="">Kandhamal Violence Update: 156 Acquitted, 1 Sentenced to Life Imprisonment</a></h3>
		<p>Multinational businesses involved in complex cross-border restructurings, workouts </p>

	</div>
	<div class="main_box04"> 
		<h2><a href="resources.php">RESOURCES</a></h2>
		<h3><a href="">Universal Declaration of Human Rights (PDF file)</a></h3>
		<!-- <p>Multinational businesses involved in complex cross-border restructurings, workouts and insolvencies require aggressive and creative representation.</p> -->
		<h3><a href="">USCIRF Annual Report 2010 (PDF file)</a></h3>
		<!-- <p>Multinational businesses involved in complex cross-border restructurings, workouts and insolvencies require aggressive and creative representation.</p> -->
		<h3><a href="">Universal Declaration of Human Rights (PDF file)</a></h3>
		<!-- <p>Multinational businesses involved in complex cross-border restructurings, workouts and insolvencies require aggressive and creative representation.</p> -->
		<h3><a href="">USCIRF Annual Report 2010 (PDF file)</a></h3>
		<!-- <p>Multinational businesses involved in complex cross-border restructurings, workouts and insolvencies require aggressive and creative representation.</p> -->
		<h3><a href="">Universal Declaration of Human Rights (PDF file)</a></h3>
		<h3><a href="">USCIRF Annual Report 2010 (PDF file)</a></h3>
		<h3><a href="">Universal Declaration of Human Rights (PDF file)</a></h3>
		<h3><a href="">USCIRF Annual Report 2010 (PDF file)</a></h3>
		<h3><a href="">Universal Declaration of Human Rights (PDF file)</a></h3>
		<h3><a href="">USCIRF Annual Report 2010 (PDF file)</a></h3>
	    </div>
	</div>
</div>
<!-- container_main end -->
	
<?php include 'part/footer.php'; ?>
</div>


<!-- scripts -->
<!-- <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script> -->
<script src="assets/js/script.js"></script>

</body>
</html>
