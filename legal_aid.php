<?php include 'part/head.php'; ?>
<body class="contianer">
<input type="checkbox" name="" id="nav-toggle" class="hidden-checkbox">
<div class="page">
	<?php include 'part/helper/no-js.php'; ?>
	<?php include 'part/helper/browsehappy.php'; ?>
	<?php include 'part/header.php'; ?>
<!-- container_main start -->		
<div class="container_article">
	<!-- aside -->
  <div data-col-aside="" class="article_box_l">
  	<div class="logo_img"><img src="assets/img/logo.jpg"></div>
  </div>
  <!-- aside end-->
	<!-- main start-->
  <div data-col-main="" class="article_box_r">
  	 <h2>Legal Aid</h2>
	  <p>An important duty of Veritas  Legal Society is to dispense legal aid.</p>
	  <p>Members of Veritas Legal  Society are mandated to offer basic legal advice and services to disadvantaged  and needy believers without charging any fees.</p>
	  <p>Our typical legal aid work  involves counseling, informal negotiation, and appearances in administrative  hearings</p>
	  <h3>Verital Legal Society legal  aid program includes but is not confined to the following fields of law:</h3>
		  <ul class="legal_list">
		  	<li>Family dispute and domestic violence</li>
		  	<li>Job/Employment</li>
		  	<li>Denial of government benefits</li>
		  	<li>Discrimination on basis of race, religion or color</li>
		  	<li>Housing</li>
		  	<li>Immigration</li>
		  	<li>Torts</li>
		  	<li>Minor crimes</li>
		  </ul>
	  <div class="info_box ">Veritas Legal Society<br />
	    529 14th St. NW Ste. 770<br />
	    Washington DC 20045<br />
	    USA</div>
  		<p class="mail">Or email us at <a href="mailto:membership@veritaslegalsociety.org">membership@veritaslegalsociety.org</a></p>

	  </div>
 </div>


  </div>
	<!-- main end-->
  </div>
	<?php include 'part/footer.php'; ?>
</div>


<!-- scripts -->
<!-- <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script> -->
<script src="assets/js/script.js"></script>

</body>
</html>
