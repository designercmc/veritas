<?php include 'part/head.php'; ?>
<body class="contianer">
<input type="checkbox" name="" id="nav-toggle" class="hidden-checkbox">
<div class="page">
	<?php include 'part/helper/no-js.php'; ?>
	<?php include 'part/helper/browsehappy.php'; ?>
	<?php include 'part/header.php'; ?>
<!-- container_main start -->		
<div class="container_article">
	<!-- aside -->
  <div data-col-aside="" class="article_box_l">
  	<div class="logo_img"><img src="assets/img/logo.jpg"></div>
  </div>
  <!-- aside end-->
	<!-- main start-->
  <div data-col-main="" class="article_box_r">
  	 <h2>Membership</h2>
  <p>Members of Veritas Legal Society primarily comprise of attorneys, solicitors, barristers, judges (both sitting in office as well as retired), judicial officers, law professors, notary public, paralegals and law students who put faith into action in their lives. However, membership is open to all believers.<br />
    <p/>
  <h3>Those interested in becoming a member of Veritas Legal Society are urged to write to us at:</h3>
  <div class="info_box ">Veritas Legal Society<br />
    529 14th St. NW Ste. 770<br />
    Washington DC 20045<br />
    USA</div>
  <p class="mail">Or email us at <a href="mailto:membership@veritaslegalsociety.org">membership@veritaslegalsociety.org<a/></p>
  <p>Applications will be screened and those approved for membership will be required to affirm and uphold the Veritas Legal Society Statement of Faith as a condition of membership.</p>
  <p>Nominal annual membership charges apply.<br />
  </p>

  </div>
	<!-- main end-->
  </div>
	<?php include 'part/footer.php'; ?>
</div>


<!-- scripts -->
<!-- <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script> -->
<script src="assets/js/script.js"></script>

</body>
</html>
