<footer class="wrap_footer">
	<div class="container_main">
		<div class="footer_main_box01">
			<div class="footer_mb">
				<img src="assets/img/logo_footer.jpg">
				<div class="footer_box">
					<div class="footer_info">
						<p>2101 L Street NW Suite 500 <br/>
						Washington, DC 20037</p>
						<p>1-800-795-1747</p>
					</div>
					<div class="social_line">
						<a href="#"><i class="i-facebook"></i></a>
						<a href="#"><i class="i-twitter"></i></a>
						<a href="#"><i class="i-google"></i></a>
					</div>
				</div>
			</div>
		</div>
		<div class="footer_main_box02">
				<div class="footer_nav">
					<ul>
						<h4><a href="who_we_are.php">Who We Are</a></h4>
						<li><a href="">What is Covered</a></li>
						<li><a href="">Plans</a></li>
						<li><a href="">How it works</a></li>
						<li><a href="">What is Covered</a></li>
					</ul>
					<ul>
						<h4><a href="what_we_do.php">What We Do</a></h4>
						<li><a href="">What is Covered</a></li>
						<li><a href="">Plans</a></li>
						<li><a href="">How it works</a></li>
						<li><a href="">What is Covered</a></li>
					</ul>
					<ul>
						<h4><a href="affiliations.php">Affiliations</a></h4>
						<li><a href="">Member Stories</a></li>
						<li><a href="">News</a></li>
					</ul>
					<ul>
						<h4><a href="membership.php">Membership</a></h4>
						<li><a href="">Member Stories</a></li>
						<li><a href="">News</a></li>
					</ul>
				</div>
				<div  class="copyright">&copy; Vritas Legal Society 2015.</div>
		</div>
		
	</div>

</footer>