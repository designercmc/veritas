<!-- header_sub start -->
<div class="img_sub"><img src="assets/img/banner_sub.jpg"></div> 

<header class="site-head-sub ">
	<div class="container">

		<!-- wrap-head-dk start -->
 		    <div class="wrap-head">
				
						<h1 class="logo"><a href="index.php"><img src="assets/img/logo-lg.png" alt="Veritas" ></a></h1>   
						 <div class="wrap-head-dk">
						 	     <nav class="nav">
	 								      <ul>
	 								        <li><a href="index.php">Home</a></li>
	 								        <li><a href="who_we_are.php">Who We Are</a></li>
	 								        <li><a href="what_we_do.php">What We Do</a></li>
	 								        <li><a href="affiliations.php">Affiliations</a></li>
	 								        <li><a href="membership.php">Membership</a></li>
	 								        <li><a href="contact_us.php">Contact Us</a></li>
	 								      </ul>
	 								    </nav>       
	 	
	 							<!-- wrap-tool start -->
	 							<div class="wrap-tool">
	 								<div class="wrap-tool-mb">
	 									<label for="nav-toggle"><i class="i-menu"></i></label>
	 									<label for="nav-toggle" class="page-overlay"></label>
	 								    
	 								    <form action="" class="head-search">
							 	           <input type="text" placeholder="Search" id="head-search" autofocus>
							 	           <input type="submit" hidefocus="true" class="hidden">
							 	           <div class="search-toggle">
							 	           	<i class="i-search"></i>
							 	           </div>
	 						            </form>
	 						         </div>   
	 	  						  </div>
	 	  						  <!-- wrap-tool end-->
					</div>    
			    </div>     
	        </div>
	<!-- site-head end -->	
</header>
<!--wrap-site-head end -->

<!-- header_sub end -->