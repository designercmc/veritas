<?php include 'part/head.php'; ?>
<body class="contianer">
<input type="checkbox" name="" id="nav-toggle" class="hidden-checkbox">
<div class="page">
	<?php include 'part/helper/no-js.php'; ?>
	<?php include 'part/helper/browsehappy.php'; ?>
	<?php include 'part/header.php'; ?>
<!-- container_main start -->		
<div class="container_article">
	<!-- aside -->
  <div data-col-aside="" class="article_box_l">
  	<div class="logo_img"><img src="assets/img/logo.jpg"></div>
  </div>
  <!-- aside end-->
	<!-- main start-->
  <div data-col-main="" class="article_box_r">
  	<h2>What we do</h2>
  <p>Veritas Legal Society is a non-denominational ministry founded by Christian lawyers to help members of the legal profession put their religious faith into action in their lives.</p>
  <p>The ministry has been created by Christian lawyers with the aim of forming a common platform that will give them a stronger, collective voice to preach and spread the Good News and also help the needy believers secure justice in their lives.</p>
  <p>Veritas Legal Society is mandated to do the following:</p>
  <p>Assist believers in the legal profession actively profess, practice and propagate their faith i.e put their faith into action</p>
  <p>Help believers in the legal profession make difficult decisions and overcome difficult situations that threaten to conflict with their faith</p>
  <p>Provide free legal advice, assistance and services to the needy believers and help them secure justice in their lives</p>
  <p>Help people overcome ignorance of law by empowering them via seminars, conferences, symposiums, legal aid clinics and various other events </p>
  <p>Help churches, Christian organizations and believers prevent and fight lawsuits</p>
  <p>Defend religious freedom and sanctity of human life and help believers attain full moral and spiritual stature </p>
  <p>Work closely with like-minded organizations, institutions and government bodies to spread legal awareness among believers, especially awareness of their rights</p>
  <p>In addition, Veritas Legal Society also publishes monthly newsletter and hold weekly prayer meetings to help advance any or all of the above-mentioned causes </p>


  </div>
	<!-- main end-->
  </div>
	<?php include 'part/footer.php'; ?>
</div>


<!-- scripts -->
<!-- <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script> -->
<script src="assets/js/script.js"></script>

</body>
</html>
