<?php include 'part/head.php'; ?>
<body class="contianer">
<input type="checkbox" name="" id="nav-toggle" class="hidden-checkbox">
<div class="page">
	<?php include 'part/helper/no-js.php'; ?>
	<?php include 'part/helper/browsehappy.php'; ?>
	<?php include 'part/header.php'; ?>
<!-- container_main start -->		
<div class="container_article">
	<!-- aside -->
  <div data-col-aside="" class="article_box_l">
  	<div class="logo_img"><img src="assets/img/logo.jpg"></div>
  </div>
  <!-- aside end-->
	<!-- main start-->
  <div data-col-main="" class="article_box_r">
  	<h2>Welcome</h2>
  <p>Welcome to Veritas Legal Society.</p>
<p>As members of the legal profession, we all know that knowledge of law increases one's understanding of public affairs while its study promotes accuracy of expression, facility in argument and skill in interpreting the written word, as well as some understanding on social values.</p>
	<p>However, what many of us don't know is that law is the cement of society and also an essential medium of change. And, as members of the legal profession, we have a better opportunity than members of other professions to become instruments of social change and not confine ourselves to serving the parochial or self-interested concerns of the bar.</p>

<p>But it is not just an opportunity. We also have a duty towards the society and must ensure that justice is delivered to everyone. Because everyone has an equal right to truth, justice and remedy, even those who don't have the means to their access.</p>

<p>More importantly, as Christians, we have a special responsibility to become "peacemakers" if we are to be called "sons of God." (Matthew 5:9). In other words, as those who have accepted Lord Jesus Christ as their Savior, it is not only our duty to live life according to the Gospel but also lay the foundation stone of social change and make a positive difference in the lives of others.</p>

<p>But many of us struggle in our faith journey, especially when it comes to practicing our faith in our professional lives. I have struggled too.</p>

<p>Hence, Veritas Legal Society has been formed - to help people like you and me live a purposeful and abundant life and practice our faith in our professional lives without sacrificing our morals, scruples, ethics or religious belief. And, unlike other law societies or associations, Veritas Legal Society strives not only to bring about a positive change in the lives of its members but also in the lives of others.</p>

<p>And, in case you think it's difficult, don't. One hundred and fifty years back, a Christian lawyer has shown us the way. "Discourage litigation. Persuade your neighbors to compromise whenever you can. As a peacemaker, the lawyer has superior opportunity of being a good man," the lawyer had said in a summation of the role of members of the legal profession. When the lawyer had surrendered himself in God's hands, God used him to reveal His glory and the lawyer went on to become a great statesman and brought about the emancipation of slaves. He was none other than Abraham Lincoln, the 16th President of the United States.
</p><br />
<p>Grace and peace to you, </p>
<p><b>Jacob Chatterjee</b><br />
	<i>Executive Director</i></p>
     
  </div>

  </div>
	<!-- main end-->
  
	<?php include 'part/footer.php'; ?>
</div>


<!-- scripts -->
<!-- <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script> -->
<script src="assets/js/script.js"></script>

</body>
</html>
